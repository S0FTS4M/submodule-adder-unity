# submodule-adder-unity

This is a bash script that adds a list of submodules to the project.

Adding Submodules
./submodule.sh -p PROJECTFOLDERNAME -s 1
 
Bu kod içerisinde PROJECTFOLDERNAME yerine proje klasörünün adını yazarsan. senin için 
/home/softsam/workspace/Unity/Forge/{PROJECTFOLDERNAME}/{PROJECTFOLDERNAME}/Assets/Libraries/Forge/"

bu path içinde sshLinksToRepo içerisinde belirttiğin her bir submodule eklenir. Eklenmesini istemediklerin varsa url başına # koyarak yorum satırı haline getir

Setting Remote url

./submodule.sh -p PROJECTFOLDERNAME -r 1

Bu kod içerisinde PROJECTFOLDERNAME yerine proje klasörünün adını yazarsan. senin için 
/home/softsam/workspace/Unity/Forge/{PROJECTFOLDERNAME}/{PROJECTFOLDERNAME}/Assets/Libraries/Forge/"

bu path içinde oluşturulmuş her bir submodule bilgisini sshLinksToRepo dosyasından okur içine girer ve o reponun remote url değerini sshLinksToRepo içerisinde verilen url olarak ayarlar

TODO:
Yol olarak verilen hardcoded path kullanıcıdan bir kereliğine alınabilir
