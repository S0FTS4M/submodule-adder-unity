addSubmodule= false
setsshURL= false
while getopts p:s:r: option
do
case "${option}"
in
p) PROJECT=${OPTARG};;
s) addSubmodule=true;;
r) setsshURL=true;;
esac
done
abspath="/home/softsam/workspace/Unity/Forge/${PROJECT}/${PROJECT}/Assets/Libraries/Forge/"
while read -r path; do
    read -r url

    #!ignore lines that starts with #
    [[ $url =~ ^#.* ]] && continue
    if [[ $setsshURL ]]
    then
        cd "${abspath}${path}"
        git remote set-url origin $url
        git remote -v
        git fetch
        git pull
        #!echo "${abspath}${path}"
        #!echo $PROJECT
    fi
    if [[ $addSubmodule ]]
    then
        git submodule add $url "${abspath}${path}"
        #!echo $url
        #!echo "${abspath}${path}"
    fi


done < sshLinksToRepo.txt
